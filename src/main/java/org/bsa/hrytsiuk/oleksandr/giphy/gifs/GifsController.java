package org.bsa.hrytsiuk.oleksandr.giphy.gifs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/gifs")
public class GifsController {

    private final GifsService gifsService;

    @Autowired
    public GifsController(GifsService gifsService) {
        this.gifsService = gifsService;
    }

    @GetMapping
    public List<String> get() {
        return gifsService.getAllGifs();
    }
}

package org.bsa.hrytsiuk.oleksandr.giphy.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.ApiException;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.ApiNoResultsException;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.GifSaveDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class GiphyApi {

    @Value(value = "${giphy.api_url}")
    private String API_URL;

    @Value(value = "${giphy.api_random_endpoint}")
    private String API_RANDOM_ENDPOINT;

    @Value(value = "${giphy.api_key}")
    private String API_KEY_VALUE;

    @Value(value = "${giphy.api_method}")
    private String API_METHOD;

    public GifSaveDto getRandomGif(String query) throws ApiException, ApiNoResultsException {
        try {
            var params = prepareRequestParams(query);
            String response = request(API_RANDOM_ENDPOINT, params);
            return getSaveDtoFromResponse(response, query);
        } catch (IOException e) {
            throw new ApiException();
        }
    }

    private Map<String, String> prepareRequestParams(String query) {
        return Map.of(
                "tag", query,
                "api_key", API_KEY_VALUE
        );
    }

    private String request(String endpoint, Map<String, String> params) throws IOException {
        HttpURLConnection connection = prepareConnection(endpoint, params);

        connection.getResponseCode();
        String body = getResponseBody(connection);
        connection.disconnect();

        return body;
    }

    private HttpURLConnection prepareConnection(String endpoint, Map<String, String> params) throws IOException {
        String urlParams = params.entrySet().stream()
                .map(entry -> entry.getKey() + "=" + entry.getValue())
                .collect(Collectors.joining("&"));
        URL url = new URL(API_URL + endpoint + "?" + urlParams);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(API_METHOD);
        return connection;
    }

    private String getResponseBody(HttpURLConnection connection) throws IOException {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            StringBuilder content = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            return content.toString();
        }
    }

    private GifSaveDto getSaveDtoFromResponse(String response, String query) throws IOException, ApiNoResultsException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode json = mapper.readValue(response, JsonNode.class);
            JsonNode root = json.get("data");

            URL url = new URL(getUrlFromJsonRoot(root));
            String id = getIdFromJsonRoot(root);

            return new GifSaveDto(url, id, query);
        } catch (MalformedURLException | NullPointerException e) {
            throw new ApiNoResultsException();
        }
    }

    private String getUrlFromJsonRoot(JsonNode root) {
        String jsonUrl = root
                .get("images")
                .get("original")
                .get("url")
                .asText();

        String urlNoPrefix = jsonUrl.substring(jsonUrl.indexOf(".giphy.com/"));
        String urlCorrectPrefix = "https://i";

        return urlCorrectPrefix + urlNoPrefix;
    }

    private String getIdFromJsonRoot(JsonNode root) {
        return root
                .get("id")
                .asText();
    }
}

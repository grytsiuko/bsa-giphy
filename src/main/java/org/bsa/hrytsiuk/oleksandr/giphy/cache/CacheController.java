package org.bsa.hrytsiuk.oleksandr.giphy.cache;

import org.bsa.hrytsiuk.oleksandr.giphy.cache.dto.CacheGenerateRequestDto;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.ApiException;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.ApiNoResultsException;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.NoSearchResultsException;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.FileSystemException;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.QueryGifPathsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cache")
public class CacheController {

    private final CacheService cacheService;

    @Autowired
    public CacheController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @GetMapping
    public List<QueryGifPathsDto> get(@RequestParam Map<String, String> params)
            throws NoSearchResultsException {

        String query = params.get("query");
        return query == null
                ? cacheService.getAll()
                : cacheService.getListByQuery(query);
    }

    @PostMapping("/generate")
    public QueryGifPathsDto generate(@RequestBody @Valid CacheGenerateRequestDto cacheGenerateRequestDto)
            throws ApiNoResultsException, ApiException, FileSystemException {

        return cacheService.generateAndGetAll(cacheGenerateRequestDto);
    }

    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete() {
        cacheService.deleteAll();
    }
}

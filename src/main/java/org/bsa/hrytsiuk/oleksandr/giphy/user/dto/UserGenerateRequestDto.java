package org.bsa.hrytsiuk.oleksandr.giphy.user.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@Data
public class UserGenerateRequestDto {
    @NotBlank
    private String query;
    private Boolean force;
}

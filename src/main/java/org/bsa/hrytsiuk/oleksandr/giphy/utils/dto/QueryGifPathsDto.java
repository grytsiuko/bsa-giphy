package org.bsa.hrytsiuk.oleksandr.giphy.utils.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class QueryGifPathsDto {
    private final String query;
    private final List<String> gifs;
}

package org.bsa.hrytsiuk.oleksandr.giphy.utils;

import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.QueryGifFilesDto;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.QueryGifPathsDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ServerStaticMapper {

    @Value(value = "${static_prefix}")
    private String STATIC_PREFIX;

    @Value(value = "${cache_directory}")
    private String CACHE_DIRECTORY;

    @Value(value = "${users_directory}")
    private String USERS_DIRECTORY;

    public String mapUserFileToPath(File file) {
        return mapFileToPath(USERS_DIRECTORY, file);
    }

    public QueryGifPathsDto mapCacheFilesDtoToPathsDto(QueryGifFilesDto queryGifFilesDto) {
        return mapFilesDtoToPathsDto(CACHE_DIRECTORY, queryGifFilesDto);
    }

    public QueryGifPathsDto mapUsersFilesDtoToPathsDto(QueryGifFilesDto queryGifFilesDto) {
        return mapFilesDtoToPathsDto(USERS_DIRECTORY, queryGifFilesDto);
    }

    private String mapFileToPath(String rootDirectory, File file) {
        File cacheFile = new File(rootDirectory);
        String relativePath = cacheFile.toURI().relativize(file.toURI()).toString();
        relativePath = relativePath.replace('\\', '/'); // for Windows path backslash
        return STATIC_PREFIX + relativePath;
    }

    private QueryGifPathsDto mapFilesDtoToPathsDto(String rootDirectory, QueryGifFilesDto queryGifFilesDto) {
        List<String> paths = queryGifFilesDto.getFiles()
                .stream()
                .map(file -> mapFileToPath(rootDirectory, file))
                .collect(Collectors.toList());
        return new QueryGifPathsDto(queryGifFilesDto.getQuery(), paths);
    }
}

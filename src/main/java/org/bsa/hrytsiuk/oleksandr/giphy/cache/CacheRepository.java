package org.bsa.hrytsiuk.oleksandr.giphy.cache;

import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.FileSystemException;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.GifSaver;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.GifScanner;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.GifSaveDto;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.QueryGifFilesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.util.*;

@Repository
public class CacheRepository {

    @Value(value = "${cache_directory}")
    private String CACHE_DIRECTORY;

    private final GifScanner gifScanner;

    private final GifSaver gifSaver;

    @Autowired
    public CacheRepository(GifScanner gifScanner, GifSaver gifSaver) {
        this.gifScanner = gifScanner;
        this.gifSaver = gifSaver;
    }

    public List<QueryGifFilesDto> getAll() {
        File cacheDirectory = new File(CACHE_DIRECTORY);
        return gifScanner.getFilesFromRoot(cacheDirectory);
    }

    public QueryGifFilesDto getByQuery(String query) {
        File cacheDirectory = new File(CACHE_DIRECTORY);
        return gifScanner.getFilesFromRootByQuery(cacheDirectory, query);
    }

    public File save(GifSaveDto saveDto) throws FileSystemException {
        File cacheDirectory = new File(CACHE_DIRECTORY);
        return gifSaver.save(cacheDirectory, saveDto);
    }

    public void deleteAll() {
        File cacheDirectoryFile = new File(CACHE_DIRECTORY);
        FileSystemUtils.deleteRecursively(cacheDirectoryFile);
    }
}

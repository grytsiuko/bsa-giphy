package org.bsa.hrytsiuk.oleksandr.giphy.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class UserGenerateResponseDto {
    private final String message;
}

package org.bsa.hrytsiuk.oleksandr.giphy.cache.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CacheGenerateRequestDto {
    @NotBlank
    private String query;
}

package org.bsa.hrytsiuk.oleksandr.giphy.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class UserHistoryDto {
    private final String date;
    private final String query;
    private final String gif;
}

package org.bsa.hrytsiuk.oleksandr.giphy.user;

import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.*;
import org.bsa.hrytsiuk.oleksandr.giphy.user.dto.*;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.QueryGifPathsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}/all")
    public List<QueryGifPathsDto> getAll(@PathVariable String id)
            throws IllegalIdException {

        validateId(id);
        return userService.getByUser(id);
    }

    @GetMapping("/{id}/history")
    public List<UserHistoryDto> getHistory(@PathVariable String id)
            throws IllegalIdException {

        validateId(id);
        return userService.getUserHistory(id);
    }

    @DeleteMapping("/{id}/history/clean")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteHistory(@PathVariable String id)
            throws IllegalIdException, FileSystemException {

        validateId(id);
        userService.cleanUserHistory(id);
    }

    @GetMapping("/{id}/search")
    public String search(@PathVariable String id,
                         @RequestParam String query,
                         @RequestParam Map<String, String> params)
            throws NoSearchResultsException, IllegalIdException {

        validateId(id);
        boolean force = params.containsKey("force");
        return userService.search(id, query, force);
    }

    @PostMapping("/{id}/generate")
    public String generate(@PathVariable String id,
                           @RequestBody @Valid UserGenerateRequestDto userGenerateRequestDto)
            throws ApiNoResultsException, IllegalIdException, ApiException, FileSystemException {

        validateId(id);
        return userService.generate(id, userGenerateRequestDto);
    }

    @DeleteMapping("/{id}/reset")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteReset(@PathVariable String id, @RequestParam Map<String, String> params)
            throws IllegalIdException {

        validateId(id);
        String query = params.get("query");
        if (query == null) {
            userService.resetRamUser(id);
        } else {
            userService.resetRamUserQuery(id, query);
        }
    }

    @DeleteMapping("/{id}/clean")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteClean(@PathVariable String id)
            throws IllegalIdException {

        validateId(id);
        userService.clean(id);
    }

    private void validateId(String id) throws IllegalIdException {
        if (!Pattern.matches("[\\w]+", id)) {
            throw new IllegalIdException();
        }
    }
}

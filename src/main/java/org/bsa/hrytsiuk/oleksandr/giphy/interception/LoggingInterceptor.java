package org.bsa.hrytsiuk.oleksandr.giphy.interception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

@Component
public class LoggingInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = LoggerFactory.getLogger(LoggingInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) {

        log.info("[" + request.getMethod() + "]" +
                request.getRequestURI() +
                getParameters(request));
        return true;
    }

    private String getParameters(HttpServletRequest request) {
        StringBuilder builder = new StringBuilder();
        Enumeration<?> parameters = request.getParameterNames();
        if (parameters != null) {
            fillParameters(builder, parameters, request);
        }
        return builder.toString();
    }

    private void fillParameters(StringBuilder builder, Enumeration<?> parameters, HttpServletRequest request) {
        while (parameters.hasMoreElements()) {
            String key = (String) parameters.nextElement();
            String value = request.getParameter(key);
            appendNextParameter(builder, key, value);
        }
    }

    private void appendNextParameter(StringBuilder builder, String key, String value) {
        builder
                .append(builder.length() > 1 ? "&" : "?")
                .append(key)
                .append("=")
                .append(value);
    }
}

package org.bsa.hrytsiuk.oleksandr.giphy.utils;

import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.FileSystemException;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.GifSaveDto;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

@Component
public class GifSaver {

    public File save(File directory, GifSaveDto saveDto) throws FileSystemException {
        try {
            File file = getFileForGif(directory, saveDto.getTag(), saveDto.getName());
            if (!file.exists()) {
                saveGifToFile(file, saveDto.getUrl());
            }
            return file;
        } catch (IOException e) {
            throw new FileSystemException();
        }
    }

    private File getFileForGif(File directory, String query, String name) {
        File queryDirectory = new File(directory, query);

        queryDirectory.mkdirs();

        String fileName = name + ".gif";
        return new File(queryDirectory, fileName);
    }

    private void saveGifToFile(File file, URL url) throws IOException {
        ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        FileChannel fileChannel = fileOutputStream.getChannel();

        fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
    }
}

package org.bsa.hrytsiuk.oleksandr.giphy.gifs;

import org.bsa.hrytsiuk.oleksandr.giphy.utils.GifScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.util.List;

@Repository
public class GifsRepository {

    @Value(value = "${users_directory}")
    private String USERS_DIRECTORY;

    private final GifScanner gifScanner;

    @Autowired
    public GifsRepository(GifScanner gifScanner) {
        this.gifScanner = gifScanner;
    }

    public List<File> getAllGifs() {
        File usersDirectory = new File(USERS_DIRECTORY);
        return gifScanner.getFilesFromAllUsersRoots(usersDirectory);
    }
}

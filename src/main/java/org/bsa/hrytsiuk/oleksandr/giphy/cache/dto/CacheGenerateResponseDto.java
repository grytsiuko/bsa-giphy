package org.bsa.hrytsiuk.oleksandr.giphy.cache.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CacheGenerateResponseDto {
    private final String message;
}

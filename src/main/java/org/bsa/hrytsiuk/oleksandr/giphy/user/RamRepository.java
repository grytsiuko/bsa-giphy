package org.bsa.hrytsiuk.oleksandr.giphy.user;

import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class RamRepository {

    private final Map<String, Map<String, Set<String>>> storage = new HashMap<>();

    public List<String> getUserPathsByQuery(String id, String query) {
        var userStorage = storage.getOrDefault(id, new HashMap<>());
        var queryUserStorage = userStorage.getOrDefault(query, new HashSet<>());

        return new ArrayList<>(queryUserStorage);
    }

    public void save(String id, String query, String name) {
        var userStorage = storage.computeIfAbsent(id, k -> new HashMap<>());
        var queryUserStorage = userStorage.computeIfAbsent(query, k -> new HashSet<>());

        queryUserStorage.add(name);
    }

    public void resetUser(String id) {
        storage.remove(id);
    }

    public void resetUserQuery(String id, String query) {
        Optional.ofNullable(storage.get(id))
                .ifPresent(map -> map.remove(query));
    }
}

package org.bsa.hrytsiuk.oleksandr.giphy.utils.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;
import java.util.List;

@AllArgsConstructor
@Data
public class QueryGifFilesDto {
    private final String query;
    private final List<File> files;
}

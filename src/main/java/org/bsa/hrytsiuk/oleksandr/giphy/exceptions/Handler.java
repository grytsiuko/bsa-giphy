package org.bsa.hrytsiuk.oleksandr.giphy.exceptions;

import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class Handler {

    @ExceptionHandler({ApiException.class})
    public ResponseEntity<Object> handleApiException() {
        return new ResponseEntity<>(
                "API server error",
                HttpStatus.BAD_GATEWAY
        );
    }

    @ExceptionHandler({ApiNoResultsException.class})
    public ResponseEntity<Object> handleApiNoResultsException() {
        return new ResponseEntity<>(
                "Gifs not found on API",
                HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler({FileSystemException.class})
    public ResponseEntity<Object> handleSavingException() {
        return new ResponseEntity<>(
                "Unable to modify files",
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    @ExceptionHandler({ForbiddenAccessException.class})
    public ResponseEntity<Object> handleForbiddenAccessException() {
        return new ResponseEntity<>(
                "No X-BSA-GIPHY header",
                HttpStatus.FORBIDDEN
        );
    }

    @ExceptionHandler({IllegalIdException.class})
    public ResponseEntity<Object> handleIllegalIdException() {
        return new ResponseEntity<>(
                "Illegal user ID - should contain a-z, A-Z, 0-9 and _",
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler({NoSearchResultsException.class})
    public ResponseEntity<Object> handleNoSearchResultsException() {
        return new ResponseEntity<>(
                "Gifs not found",
                HttpStatus.NOT_FOUND
        );
    }
}

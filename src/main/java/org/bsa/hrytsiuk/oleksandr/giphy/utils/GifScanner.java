package org.bsa.hrytsiuk.oleksandr.giphy.utils;

import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.QueryGifFilesDto;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class GifScanner {

    public List<File> getFilesFromAllUsersRoots(File directory) {
        return getChildrenFilesStream(directory)
                .filter(File::isDirectory)
                .map(this::getFilesFromRoot)
                .flatMap(Collection::stream)
                .map(QueryGifFilesDto::getFiles)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public List<QueryGifFilesDto> getFilesFromRoot(File directory) {
        return getChildrenFilesStream(directory)
                .filter(File::isDirectory)
                .map(this::getFilesFromQueryDirectory)
                .collect(Collectors.toList());
    }

    public QueryGifFilesDto getFilesFromRootByQuery(File directory, String query) {
        File queryDirectory = new File(directory, query);
        return getFilesFromQueryDirectory(queryDirectory);
    }

    private QueryGifFilesDto getFilesFromQueryDirectory(File directory) {
        List<File> files = getChildrenFilesStream(directory)
                .filter(File::isFile)
                .filter(file -> file.getName().endsWith(".gif"))
                .collect(Collectors.toList());

        return new QueryGifFilesDto(directory.getName(), files);
    }

    private Stream<File> getChildrenFilesStream(File file) {
        File[] children = file.listFiles();

        if (children == null || children.length == 0) {
            return Stream.empty();
        }
        return Arrays.stream(children);
    }

}

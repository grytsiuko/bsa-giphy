package org.bsa.hrytsiuk.oleksandr.giphy.user;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.FileSystemException;
import org.bsa.hrytsiuk.oleksandr.giphy.user.dto.UserHistoryDto;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.GifScanner;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.QueryGifFilesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileSystemUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
public class UserRepository {

    @Value(value = "${users_directory}")
    private String USERS_DIRECTORY;

    @Value(value = "${users_history_file_name}")
    private String USERS_HISTORY_FILE_NAME;

    private final GifScanner gifScanner;

    @Autowired
    public UserRepository(GifScanner gifScanner) {
        this.gifScanner = gifScanner;
    }

    public List<QueryGifFilesDto> getUserFiles(String id) {
        File userDirectory = Path.of(USERS_DIRECTORY, id).toFile();
        return gifScanner.getFilesFromRoot(userDirectory);
    }

    public QueryGifFilesDto getUserFilesByQuery(String id, String query) {
        File userDirectory = Path.of(USERS_DIRECTORY, id).toFile();
        return gifScanner.getFilesFromRootByQuery(userDirectory, query);
    }

    public List<UserHistoryDto> getUserHistory(String id) {
        try {
            File historyFile = Path.of(USERS_DIRECTORY, id, USERS_HISTORY_FILE_NAME).toFile();
            return readDirectoryHistory(historyFile);
        } catch (IOException e) {
            return Collections.emptyList();
        }
    }

    public List<UserHistoryDto> readDirectoryHistory(File file) throws IOException {
        List<UserHistoryDto> list = new ArrayList<>();
        CSVReader csvReader = new CSVReader(new FileReader(file), ',', '"', 0);

        String[] nextLine;
        while ((nextLine = csvReader.readNext()) != null) {
            if (nextLine.length == 3) {
                list.add(new UserHistoryDto(nextLine[0], nextLine[1], nextLine[2]));
            }
        }
        return list;
    }

    public boolean has(String id, String tag, String fileName) {
        File file = Path.of(USERS_DIRECTORY, id, tag, fileName).toFile();
        return file.exists();
    }

    public File add(String id, String query, File cacheFile) throws FileSystemException {
        try {
            File gifFile = prepareFileToSave(id, query, cacheFile.getName());
            if (!gifFile.exists()) {
                Files.copy(cacheFile.toPath(), gifFile.toPath());
            }
            return gifFile;
        } catch (IOException e) {
            throw new FileSystemException();
        }
    }

    private File prepareFileToSave(String id, String query, String name) {
        File userQueryDirectory = Path.of(USERS_DIRECTORY, id, query).toFile();

        userQueryDirectory.mkdirs();
        return new File(userQueryDirectory, name);
    }

    public void appendHistory(String id, String query, String filePath) throws FileSystemException {
        File historyFile = Path.of(USERS_DIRECTORY, id, USERS_HISTORY_FILE_NAME).toFile();

        try (CSVWriter csvWriter = prepareCsvWriter(historyFile)) {
            String[] data = prepareHistoryData(query, filePath);
            csvWriter.writeNext(data);
        } catch (IOException e) {
            throw new FileSystemException();
        }
    }

    private CSVWriter prepareCsvWriter(File file) throws IOException {
        return file.exists()
                ? new CSVWriter(new FileWriter(file, true))
                : new CSVWriter(new FileWriter(file));
    }

    private String[] prepareHistoryData(String query, String filePath) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDateTime now = LocalDateTime.now();

        return new String[]{dtf.format(now), query, filePath};
    }

    public void clean(String id) {
        File currentUserDirectoryFile = Path.of(USERS_DIRECTORY, id).toFile();
        FileSystemUtils.deleteRecursively(currentUserDirectoryFile);
    }

    public void cleanUserHistory(String id) throws FileSystemException {
        File historyFile = Path.of(USERS_DIRECTORY, id, USERS_HISTORY_FILE_NAME).toFile();

        if (historyFile.exists() && !historyFile.delete()) {
            throw new FileSystemException();
        }
    }
}

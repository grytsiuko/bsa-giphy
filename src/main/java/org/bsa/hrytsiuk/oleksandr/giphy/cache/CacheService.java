package org.bsa.hrytsiuk.oleksandr.giphy.cache;

import org.bsa.hrytsiuk.oleksandr.giphy.api.GiphyApi;
import org.bsa.hrytsiuk.oleksandr.giphy.cache.dto.CacheGenerateRequestDto;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.ApiException;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.ApiNoResultsException;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.NoSearchResultsException;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.FileSystemException;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.ServerStaticMapper;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.QueryGifPathsDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CacheService {

    private static final Logger log = LoggerFactory.getLogger(CacheService.class);

    private final GiphyApi giphyApi;

    private final CacheRepository cacheRepository;

    private final ServerStaticMapper serverStaticMapper;

    @Autowired
    public CacheService(GiphyApi giphyApi, CacheRepository cacheRepository, ServerStaticMapper serverStaticMapper) {
        this.giphyApi = giphyApi;
        this.cacheRepository = cacheRepository;
        this.serverStaticMapper = serverStaticMapper;
    }

    public List<QueryGifPathsDto> getAll() {
        var result = cacheRepository.getAll();
        return result
                .stream()
                .map(serverStaticMapper::mapCacheFilesDtoToPathsDto)
                .collect(Collectors.toList());
    }

    public List<QueryGifPathsDto> getListByQuery(String query) throws NoSearchResultsException {
        var result = getByQuery(query);
        if (result.getGifs().isEmpty()) {
            throw new NoSearchResultsException();
        }
        return List.of(result);
    }

    private QueryGifPathsDto getByQuery(String query) {
        var result = cacheRepository.getByQuery(query);
        return serverStaticMapper.mapCacheFilesDtoToPathsDto(result);
    }

    public QueryGifPathsDto generateAndGetAll(CacheGenerateRequestDto cacheGenerateRequestDto)
            throws ApiNoResultsException, FileSystemException, ApiException {

        generateNewFile(cacheGenerateRequestDto);
        return getByQuery(cacheGenerateRequestDto.getQuery());
    }

    public File generateNewFile(CacheGenerateRequestDto cacheGenerateRequestDto)
            throws ApiNoResultsException, ApiException, FileSystemException {

        log.info("API request invoked");
        String query = cacheGenerateRequestDto.getQuery();
        var saveDto = giphyApi.getRandomGif(query);
        var result = cacheRepository.save(saveDto);
        log.info("Gif saved to cache");
        return result;
    }

    public void deleteAll() {
        cacheRepository.deleteAll();
    }
}

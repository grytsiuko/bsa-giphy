package org.bsa.hrytsiuk.oleksandr.giphy.user;

import org.bsa.hrytsiuk.oleksandr.giphy.cache.CacheRepository;
import org.bsa.hrytsiuk.oleksandr.giphy.cache.CacheService;
import org.bsa.hrytsiuk.oleksandr.giphy.cache.dto.CacheGenerateRequestDto;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.ApiException;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.ApiNoResultsException;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.NoSearchResultsException;
import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.FileSystemException;
import org.bsa.hrytsiuk.oleksandr.giphy.user.dto.UserGenerateRequestDto;
import org.bsa.hrytsiuk.oleksandr.giphy.user.dto.UserHistoryDto;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.ServerStaticMapper;
import org.bsa.hrytsiuk.oleksandr.giphy.utils.dto.QueryGifPathsDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class UserService {

    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    private final CacheService cacheService;

    private final CacheRepository cacheRepository;

    private final UserRepository userRepository;

    private final RamRepository ramRepository;

    private final ServerStaticMapper serverStaticMapper;

    @Autowired
    public UserService(CacheService cacheService, CacheRepository cacheRepository,
                       UserRepository userRepository, RamRepository ramRepository,
                       ServerStaticMapper serverStaticMapper) {
        this.cacheService = cacheService;
        this.cacheRepository = cacheRepository;
        this.userRepository = userRepository;
        this.ramRepository = ramRepository;
        this.serverStaticMapper = serverStaticMapper;
    }

    public List<QueryGifPathsDto> getByUser(String id) {
        var result = userRepository.getUserFiles(id);
        return result
                .stream()
                .map(serverStaticMapper::mapUsersFilesDtoToPathsDto)
                .collect(Collectors.toList());
    }

    public List<UserHistoryDto> getUserHistory(String id) {
        return userRepository.getUserHistory(id);
    }

    public String search(String id, String query, boolean force) throws NoSearchResultsException {
        if (!force) {
            var gifs = ramRepository.getUserPathsByQuery(id, query);
            if (!gifs.isEmpty()) {
                log.info("Result fetched from RAM");
                return getRandomItemFromList(gifs);
            }
        }
        return searchOnDisk(id, query);
    }

    private String searchOnDisk(String id, String query) throws NoSearchResultsException {
        var diskFiles = userRepository.getUserFilesByQuery(id, query).getFiles();
        if (diskFiles.isEmpty()) {
            throw new NoSearchResultsException();
        }

        File file = getRandomItemFromList(diskFiles);
        String path = serverStaticMapper.mapUserFileToPath(file);
        log.info("Result fetched from disk storage");
        ramRepository.save(id, query, path);
        log.info("Gif saved to RAM");
        return path;
    }

    private <T> T getRandomItemFromList(List<T> list) {
        Random rand = new Random();
        return list.get(rand.nextInt(list.size()));
    }

    public String generate(String id, UserGenerateRequestDto requestDto)
            throws ApiNoResultsException, FileSystemException, ApiException {

        String query = requestDto.getQuery();
        Boolean force = requestDto.getForce();

        if (force == null || !force) {
            var cacheFiles = cacheRepository.getByQuery(query).getFiles();

            if (!cacheFiles.isEmpty()) {
                for (File cacheFile : cacheFiles) {
                    if (!userRepository.has(id, query, cacheFile.getName())) {
                        return copyFileFromCache(id, query, cacheFile);
                    }
                }
            }
        }

        return generateNewAndCopy(id, query);
    }

    private String generateNewAndCopy(String id, String query)
            throws FileSystemException, ApiException, ApiNoResultsException {

        var requestDto = new CacheGenerateRequestDto(query);
        File cacheFile = cacheService.generateNewFile(requestDto);
        return copyFileFromCache(id, query, cacheFile);
    }

    private String copyFileFromCache(String id, String query, File cacheFile) throws FileSystemException {
        File newFile = userRepository.add(id, query, cacheFile);
        log.info("Gif saved to user folder");
        String newPath = serverStaticMapper.mapUserFileToPath(newFile);
        userRepository.appendHistory(id, query, newPath);
        ramRepository.save(id, query, newPath);
        log.info("Gif saved to RAM");
        return newPath;
    }

    public void cleanUserHistory(String id) throws FileSystemException {
        userRepository.cleanUserHistory(id);
    }

    public void resetRamUser(String id) {
        ramRepository.resetUser(id);
    }

    public void resetRamUserQuery(String id, String query) {
        ramRepository.resetUserQuery(id, query);
    }

    public void clean(String id) {
        ramRepository.resetUser(id);
        userRepository.clean(id);
    }
}

package org.bsa.hrytsiuk.oleksandr.giphy.gifs;

import org.bsa.hrytsiuk.oleksandr.giphy.utils.ServerStaticMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GifsService {

    private final GifsRepository gifsRepository;

    private final ServerStaticMapper serverStaticMapper;

    @Autowired
    public GifsService(GifsRepository gifsRepository, ServerStaticMapper serverStaticMapper) {
        this.gifsRepository = gifsRepository;
        this.serverStaticMapper = serverStaticMapper;
    }

    public List<String> getAllGifs() {
        var result = gifsRepository.getAllGifs();
        return result
                .stream()
                .map(serverStaticMapper::mapUserFileToPath)
                .collect(Collectors.toList());
    }
}

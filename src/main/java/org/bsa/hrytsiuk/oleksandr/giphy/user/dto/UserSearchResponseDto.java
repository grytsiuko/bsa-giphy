package org.bsa.hrytsiuk.oleksandr.giphy.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class UserSearchResponseDto {
    private final String message;
}

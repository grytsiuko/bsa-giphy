package org.bsa.hrytsiuk.oleksandr.giphy.interception;

import org.bsa.hrytsiuk.oleksandr.giphy.exceptions.entities.ForbiddenAccessException;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {

        if (request.getHeader("X-BSA-GIPHY") == null) {
            throw new ForbiddenAccessException();
        }
        return true;
    }
}

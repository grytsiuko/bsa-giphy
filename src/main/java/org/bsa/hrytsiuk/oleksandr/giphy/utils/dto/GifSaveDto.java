package org.bsa.hrytsiuk.oleksandr.giphy.utils.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URL;

@Data
@AllArgsConstructor
public class GifSaveDto {
    private final URL url;
    private final String name;
    private final String tag;
}
